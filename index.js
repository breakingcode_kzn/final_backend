
const Config = require('./Config');
const DIContainer = require('./DIContainer');
const express = require('express');
const cors = require('cors');
const http = require('http');
const morgan = require('morgan');

const app = express();
const server = http.createServer(app);

const env = app.get('env');
console.log(env);


/**
 * Middleware
 */
app.use(cors()); // TODO: debug condition
app.use(morgan('dev', { immediate: true, stream: process.stdout }));
app.use(express.json());
app.use(express.static('static'));


/**
 * Controllers
 */
const requestController = DIContainer.cradle.requestController;
app.post('/request/row', (req, res) => requestController.createRowRequest(req, res));

app.post('/request/:id/:status', (req, res) => requestController.changeStatus(req, res));
app.get('/request/:id', (req, res) => requestController.getRequestById(req, res));
app.post('/request/:id', (req, res) => requestController.update(req, res));
app.get('/request/status/:status', (req, res) => requestController.getRequestsByStatus(req, res));
app.get('/request/subcategory/:subcategory', (req, res) => requestController.getRequestsBySubcategory(req, res));
app.get('/request/', (req, res) => requestController.getAllRequests(req, res));
app.post('/request/', (req, res) => requestController.createRequest(req, res));


const categoryController = DIContainer.cradle.categoryController;
app.get('/category/', (req, res) => categoryController.getAllCategories(req, res));
app.get('/subcategory/:category', (req, res) => categoryController.getSubcategoriesByCategory(req, res));
app.get('/type/:subcategory', (req, res) => categoryController.getTypesBySubcategry(req, res));

/**
 * Run
 */
app.addListener("close", async () => {
    await DIContainer.dispose()
        .then(() => {
            console.log('All dependencies disposed, you can exit now')
        })
        .catch((e) => console.error(e));
});
const port = Config.webServer.APP_PORT;
server.listen(port, () => console.log(`Application listening at port ${port}`));