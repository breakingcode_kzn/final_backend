const Request = require('./Request');

const TABLE_NAME = 'request';
const SCHEMA = 'public';

module.exports = class RequestRepository {
    constructor({ datasource }) {
        this.datasource = datasource;
    }

    init() {
        const CONDITION = `SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema='${SCHEMA}' AND table_name='${TABLE_NAME}');`;
        const START_SCRIPTS = [
            `create table ${SCHEMA}.${TABLE_NAME}(id uuid, status varchar, priority varchar, category varchar, address varchar, performer varchar, author varchar, add_info varchar);`,
        ];
        this.datasource
            .transaction((client) => {
                return client
                    .query(CONDITION)
                    .then(async result => {
                        console.dir(result);
                        if (result && result.rows && result.rows[0] && !result.rows[0].exists) {
                            console.log('Data does not exist');
                            for (let script of START_SCRIPTS) {
                                console.log(`Query is ${script}`);
                                await client
                                        .query(script)
                                        .catch((e) => {
                                            console.error(e);
                                        });
                            }
                        } else {
                            console.log('Data already exists');
                        }
                    })
            })
            .catch((e) => {
                console.error(e);
            });
    }
    
    getAllRequests() {
        return this.datasource.query(`SELECT * FROM ${SCHEMA}.${TABLE_NAME}`)
            .then(result => {
                return result.map(row => Request.restore(row, this));
            });
    }
    
    getRequestById(id) {
        return this.datasource.query(`SELECT * FROM ${SCHEMA}.${TABLE_NAME} WHERE id='${id}'`)
            .then(result => {
                return result.map(row => Request.restore(row, this));
            })
            .then(result => {console.log(result);return result;})
            .then(result => result && Array.isArray(result) && result.length > 0 ? result[0] : null);
    }

    getRequestsByStatus(status) {
        return this.datasource.query(`SELECT * FROM ${SCHEMA}.${TABLE_NAME} WHERE status='${status}'`)
        .then(result => {
            return result.map(row => Request.restore(row, this));
        });
    }

    getRequestsBySubcategory(subcategoryId) {
        return this.datasource.query(
            `SELECT distinct r.id, r.status, r.category, r.address, r.performer, r.author, r.priority, r.add_info ` + 
            `FROM ${SCHEMA}.${TABLE_NAME} as r join ${SCHEMA}.type as t on r.category=t.name join ${SCHEMA}.subcategory_type as st on t.id=st.type ` + 
            `WHERE st.subcategory='${subcategoryId}'`
        )
        .then(result => {
            return result.map(row => Request.restore(row, this));
        });
    }

    create(request) {
        let query = `INSERT INTO ${SCHEMA}.${TABLE_NAME}( id, status, category, priority, performer, address, author, add_info) ` + 
        `VALUES('${request.id}', '${request.status.code}', '${request.category}', '${request.priority}', '${request.performer}', '${request.address}', '${request.author}', '${request.additionalInfo}');`
        ;
        return this.datasource.query(query);
    }

    update(request) {
        return this.datasource.transaction((client) => client.query(
            `UPDATE ${SCHEMA}.${TABLE_NAME} ` + 
            `SET status='${request.status.code}', address='${request.address}', author='${request.author}', add_info='${request.additionalInfo}', category='${request.category}', priority='${request.priority}' ` + 
            `WHERE id='${request.id}'`
        ));
    }

    sendToPerformer(request) {
        return this._updateStatus(request.id, request.status);
    }

    inProgress(request) {
        return this._updateStatus(request.id, request.status);
    }

    done(request) {
        return this._updateStatus(request.id, request.status);
    }

    cancel(request) {
        return this._updateStatus(request.id, request.status);
    }
    
    close(request) {
        return this._updateStatus(request.id, request.status);
    }

    _updateStatus(id, status) {
        return this.datasource.transaction((client) => client.query(
            `UPDATE ${SCHEMA}.${TABLE_NAME} ` + 
            `SET status='${status.code}' ` + 
            `WHERE id='${id}'`
        ));
    }
}