const Requst = require('./Category');

module.exports = class CategoryController {

    constructor({ categoryRepository }) {
        this.categoryRepository = categoryRepository;
    }

    getAllCategories(req, res) {
        return this.categoryRepository.getAllCategories()
                .then(result => result.map(category => ({...category})))
                .then((result) => {
                    console.log(result);
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end(error);
                });
    }

    getSubcategoriesByCategory(req, res) {
        return this.categoryRepository.getSubcategoriesByCategory(req.params.category)
                .then(result => {
                    console.log('RESULT', result);
                    return result.map(request => ({...request}));
                })
                .then((result) => {
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end(error);
                });
    }

    getTypesBySubcategry(req, res) {
        return this.categoryRepository.getTypesBySubcategry(req.params.subcategory)
                .then(result => {
                    console.log('RESULT', result);
                    return result.map(request => ({...request}));
                })
                .then((result) => {
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end(error);
                });
    }
};