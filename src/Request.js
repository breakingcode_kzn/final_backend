const uuidv4 = require('uuid/v4');

module.exports = class Request {
    
    static createNew({category, address, priority, author, additionalInfo, requestRepository, categoryRepository}) {
        console.log(arguments);
        if (!category || !address || !author || !priority || !requestRepository || !categoryRepository) {
            throw new Error('Не заполнено одно из обязательных полей (category, priority, address, author, requestRepository, categoryRepository)');
        }
        let request = new Request();

        request.id = uuidv4();
        request.status = Status.CREATED;
        request.performer = null;
        request.category = category;
        request.priority = priority;
        request.address = address;
        request.author = author;
        request.additionalInfo = additionalInfo;
    
        request.requestRepository = requestRepository;
        request.categoryRepository = categoryRepository;

        return categoryRepository.getPriorityByTypeName(category)
            .then((priority) => {
                request.priority = priority;
                return request.onCreated();
            })
            .then(() => request);
    }
    
    static createAuto({category, additionalInfo, requestRepository, categoryRepository}) {
        console.log(arguments);
        if (!category || !additionalInfo || !requestRepository || !categoryRepository) {
            throw new Error('Не заполнено одно из обязательных полей (category, additionalInfo, requestRepository, categoryRepository)');
        }
        let request = new Request();

        request.id = uuidv4();
        request.status = Status.CREATED;
        request.performer = null;
        request.category = category;
        request.priority = null;
        request.address = null;
        request.author = 'AUTO';
        request.additionalInfo = additionalInfo;
    
        request.requestRepository = requestRepository;

        return categoryRepository.getPriorityByTypeName(category)
            .then((priority) => {
                request.priority = priority;
                return request.onCreated();
            })
            .then(() => request);
    }
    
    static update({category, address, priority, author, additionalInfo, status, performer, id, requestRepository}) {
        console.log(arguments);
        if (!category || !address || !author || !priority || !requestRepository) {
            throw new Error('Не заполнено одно из обязательных полей (category, priority, address, author, requestRepository)');
        }
        let request = new Request();

        request.id = id;
        request.status = Status.byCode(status);
        request.performer = performer;
        request.category = category;
        request.priority = priority;
        request.address = address;
        request.author = author;
        request.additionalInfo = additionalInfo;
    
        request.requestRepository = requestRepository;

        return request.onUpdate()
            .then(() => request);
    }

    static restore(data, repository) {

        let request = new Request();

        request.id = data.id;
        request.status = Status.byCode(data.status);
        request.performer = data.performer;
        request.category = data.category;
        request.priority = data.priority;
        request.performer = data.performer;
        request.address = data.address;
        request.author = data.author;
        request.additionalInfo = data.add_info;
    
        request.requestRepository = repository;

        return request;
    }
    
    constructor() {
        this.id = undefined;
        this.status = undefined;
        this.performer = undefined;
        this.category = undefined;
        this.priority = undefined;
        this.address = undefined;
        this.author = undefined;
        this.additionalInfo = undefined;

        this.requestRepository = undefined;
    }

    changeStatus(newStatus) {
        return Promise.resolve()
            .then(() => {
            if (!newStatus) {
                throw new Error('Не указан новый статус');
            }
            console.log(newStatus);
            this.status = Status.byCode(newStatus);
            switch (newStatus) {
                case Status.SENT_TO_PERFORMER.code: {
                    return this.onSendToPerformer();
                }
                case Status.IN_PROGRESS.code: {
                    return this.onInProgress();
                }
                case Status.DONE.code: {
                    return this.onDone();
                }
                case Status.CANCELLED.code: {
                    return this.onCancelled();
                }
                case Status.CLOSED.code: {
                    return this.onClosed();
                }
                default: {
                    throw new Error('Новый статус не распознан');
                }
            }
        });
    }

    /* **************** */

    onCreated() {
        return this.requestRepository.create(this);
    }

    onSendToPerformer() {
        return this.requestRepository.sendToPerformer(this);
    }

    onInProgress() {
        return this.requestRepository.inProgress(this);
    }

    onDone() {
        return this.requestRepository.done(this);
    }

    onCancelled() {
        return this.requestRepository.cancel(this);
    }

    onClosed() {
        return this.requestRepository.close(this);
    }

    onUpdate() {
        return this.requestRepository.update(this);
    }
}

class Status {
    static get CREATED() {
        return new Status('CREATED', 'Создана');
    }
    static get SENT_TO_PERFORMER() {
        return new Status('SENT_TO_PERFORMER', 'Передана исполнителю');
    } 
    static get IN_PROGRESS() {
        return new Status('IN_PROGRESS', 'В работе'); 
    }
    static get DONE() {
        return new Status('DONE', 'Выполнена'); 
    }
    static get CANCELLED() {
        return new Status('CANCELLED', 'Отменена'); 
    }
    static get CLOSED() {
        return new Status('CLOSED', 'Закрыта'); 
    }
    
    static byCode(code) {
        if (!code) {
            throw new Error('Код статуса не может быть пустым');
        }
        switch (code) {
            case this.CREATED.code: {
                return this.CREATED;
            }
            case this.SENT_TO_PERFORMER.code: {
                return this.SENT_TO_PERFORMER;
            }
            case this.IN_PROGRESS.code: {
                return this.IN_PROGRESS;
            }
            case this.DONE.code: {
                return this.DONE;
            }
            case this.CANCELLED.code: {
                return this.CANCELLED;
            }
            case this.CLOSED.code: {
                return this.CLOSED;
            }
            default: {
                throw new Error(`Не найден соответствующий статус для кода ${code}`);
            }
        }
    }

    constructor(code, name) {
        this.code = code;
        this.name = name;
    }
}