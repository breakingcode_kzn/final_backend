const {Category, SubCategory, Type} = require('./Category');
const uuidv4 = require('uuid/v4');

const CATEGORY_TABLE_NAME = 'category';
const SUBCATEGORY_TABLE_NAME = 'subcategory';
const TYPE_TABLE_NAME = 'type';
const SCHEMA = 'public';

module.exports = class CategoryRepository {
    constructor({ datasource }) {
        this.datasource = datasource;
    }

    init() {
        const CONDITION = `SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema='${SCHEMA}' AND table_name='${CATEGORY_TABLE_NAME}');`;
        
        const types = [
            {id: '230f0a49-9f30-4809-9c55-ec7fdeccbe5f', name: '1.01 Прорыв трубы'},
            {id: '120f0a49-0230-1809-9c55-ec72de1cbe5f', name: '2.01 Неисправный лифт'},
            {id: '390f0a46-4f30-7809-7c57-2c7fdec0be50', name: '31.03 Короткое замыкание'},
        ];
        const subcategories = [
            {id: '23020249-9f20-2809-9c55-6c7f6e6cbe5f', name: '01 Холодное водоснабжение', style: 'vdsnbzh', defaultPriority: 'MEDIUM'},
            {id: '15050a49-6230-1809-6c55-e662de1abe5a', name: '02 Лифт', style: 'lift', defaultPriority: 'LOW'},
            {id: '110f0a46-2f30-7849-4c57-5c7fdec0be5c', name: '31 Электротехническое оборудование', style: 'el', defaultPriority: 'HIGH'},
        ];
        const categories = [
            {id: '20180a39-9f40-4304-9c55-ec7f3eccbe5f', name: 'Эксплуатация жилого фонда'},
            {id: '180f0a48-0230-1809-9c55-ec724e1cbe4f', name: 'Водоотведение до ввода в МКД (только для РСО)'},
        ];
        
        const START_SCRIPTS = [
            `create table ${SCHEMA}.${CATEGORY_TABLE_NAME}(id uuid, name varchar);`,
            `create table ${SCHEMA}.${CATEGORY_TABLE_NAME}_${SUBCATEGORY_TABLE_NAME}(id uuid, category uuid, subcategory uuid);`,
            `create table ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}(id uuid, name varchar, style varchar, default_priority varchar);`,
            `create table ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME}(id uuid, subcategory uuid, type uuid);`,
            `create table ${SCHEMA}.${TYPE_TABLE_NAME}(id uuid, name varchar);`,

            
            `insert into ${SCHEMA}.${TYPE_TABLE_NAME}(id, name) VALUES('${types[0].id}', '${types[0].name}');`,
            `insert into ${SCHEMA}.${TYPE_TABLE_NAME}(id, name) VALUES('${types[1].id}', '${types[1].name}');`,
            `insert into ${SCHEMA}.${TYPE_TABLE_NAME}(id, name) VALUES('${types[2].id}', '${types[2].name}');`,
            
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}(id, name, style, default_priority) VALUES('${subcategories[0].id}', '${subcategories[0].name}', '${subcategories[0].style}', '${subcategories[0].defaultPriority}');`,
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}(id, name, style, default_priority) VALUES('${subcategories[1].id}', '${subcategories[1].name}', '${subcategories[1].style}', '${subcategories[1].defaultPriority}');`,
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}(id, name, style, default_priority) VALUES('${subcategories[2].id}', '${subcategories[2].name}', '${subcategories[2].style}', '${subcategories[2].defaultPriority}');`,
            
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME}(id, subcategory, type) VALUES('2302f249-9f20-2809-9c55-6c7f6e6cbe5f', '${subcategories[0].id}', '${types[0].id}');`,
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME}(id, subcategory, type) VALUES('1c05aa4c-623c-180c-6cc5-e6c2de1ace5a', '${subcategories[1].id}', '${types[1].id}');`,
            `insert into ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME}(id, subcategory, type) VALUES('1edf0a46-df30-7849-4c57-dc7fdec0be5d', '${subcategories[2].id}', '${types[2].id}');`,
            
            `insert into ${SCHEMA}.${CATEGORY_TABLE_NAME}(id, name) VALUES('${categories[0].id}', '${categories[0].name}');`,
            `insert into ${SCHEMA}.${CATEGORY_TABLE_NAME}(id, name) VALUES('${categories[1].id}', '${categories[1].name}');`,

            
            `insert into ${SCHEMA}.${CATEGORY_TABLE_NAME}_${SUBCATEGORY_TABLE_NAME}(id, subcategory, category) VALUES('f302f249-9f20-2809-9c55-6c7a6e6cbe5f', '${subcategories[0].id}', '${categories[0].id}');`,
            `insert into ${SCHEMA}.${CATEGORY_TABLE_NAME}_${SUBCATEGORY_TABLE_NAME}(id, subcategory, category) VALUES('ac05aa4c-6a3c-180c-6cc5-eacade1ace5a', '${subcategories[1].id}', '${categories[0].id}');`,
            `insert into ${SCHEMA}.${CATEGORY_TABLE_NAME}_${SUBCATEGORY_TABLE_NAME}(id, subcategory, category) VALUES('1edfea46-df30-7849-4c57-df7fdec0be2d', '${subcategories[2].id}', '${categories[1].id}');`,
        ];
        this.datasource
            .transaction((client) => {
                return client
                    .query(CONDITION)
                    .then(async result => {
                        console.dir(result);
                        if (result && result.rows && result.rows[0] && !result.rows[0].exists) {
                            console.log('Data does not exist');
                            for (let script of START_SCRIPTS) {
                                console.log(`Query is ${script}`);
                                await client
                                        .query(script)
                                        .catch((e) => {
                                            console.error(e);
                                        });
                            }
                        } else {
                            console.log('Data already exists');
                        }
                    })
            })
            .catch((e) => {
                console.error(e);
            });
    }
    
    getAllCategories() {
        return this.datasource.query(`SELECT * FROM ${SCHEMA}.${CATEGORY_TABLE_NAME}`)
            .then(result => {
                let _res = result.map(async row => {
                    let subcategories = await this.getSubcategoriesByCategory(row.id);
                    return new Category(row.id, row.name, subcategories);
                });
                return Promise.all(_res);
            });
    }
    
    getSubcategoriesByCategory(categoryId) {
        return this.datasource.query(
            `SELECT distinct s.id, s.name, s.style, s.default_priority ` + 
            `FROM ${SCHEMA}.${SUBCATEGORY_TABLE_NAME} as s join ${SCHEMA}.${CATEGORY_TABLE_NAME}_${SUBCATEGORY_TABLE_NAME} as _link on s.id=_link.subcategory ` +
            `WHERE _link.category='${categoryId}'`
        )
            .then(result => {
                let _res = result.map(async row => {
                    let types = await this.getTypesBySubcategry(row.id);
                    return new SubCategory(row.id, row.name, types, row.style, row.default_priority);
                });
                return Promise.all(_res);
            });
    }

    getTypesBySubcategry(subcategoryId) {
        return this.datasource.query(
            `SELECT t.id, t.name ` + 
            `FROM ${SCHEMA}.${TYPE_TABLE_NAME} as t join ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME} as _link on t.id=_link.type ` +
            `WHERE _link.subcategory='${subcategoryId}'`
        )
        .then(result => {
            return result.map(row => new Type(row.id, row.name));
        });;
    }

    getPriorityByTypeName(typeName) {
        return this.datasource.query(
            `SELECT distinct s.default_priority ` + 
            `FROM ${SCHEMA}.${TYPE_TABLE_NAME} as t join ${SCHEMA}.${SUBCATEGORY_TABLE_NAME}_${TYPE_TABLE_NAME} as _link on t.id=_link.type join ${SCHEMA}.${SUBCATEGORY_TABLE_NAME} as s on s.id=_link.subcategory ` +
            `WHERE t.name='${typeName}'`
        )
        .then(res => {
            return res && res.length > 0 ? res[0].default_priority : null
        });
    }
}