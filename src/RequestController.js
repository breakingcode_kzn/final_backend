const Requst = require('./Request');

module.exports = class RequestController {

    constructor({ categoryRepository, requestRepository, classifier }) {
        this.categoryRepository = categoryRepository;
        this.requestRepository = requestRepository;
        this.classifier = classifier;
    }

    getAllRequests(req, res) {
        return this.requestRepository.getAllRequests()
                .then(result => result.map(request => ({...request, requestRepository: null})))
                .then((result) => {
                    console.log(result);
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    getRequestById(req, res) {
        return this.requestRepository.getRequestById(req.params.id)
                .then(request => ({...request, requestRepository: null}))
                .then((result) => {
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    getRequestsByStatus(req, res) {
        return this.requestRepository.getRequestsByStatus(req.params.status)
                .then(result => result.map(request => ({...request, requestRepository: null})))
                .then((result) => {
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    getRequestsBySubcategory(req, res) {
        return this.requestRepository.getRequestsBySubcategory(req.params.subcategory)
                .then(result => result.map(request => ({...request, requestRepository: null})))
                .then((result) => {
                    res.json(result);
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    createRequest(req, res) {
        let requestDto = req.body;

        console.log(requestDto);

        return Promise.resolve()
                .then(() => Requst.createNew({...requestDto, requestRepository: this.requestRepository, categoryRepository: this.categoryRepository}))
                .then((request) => {
                    res.json({id: request.id});
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    update(req, res) {
        let requestDto = req.body;
        let requestId = req.params.id;

        console.log(requestDto);

        return Promise.resolve()
                .then(() => Requst.update({...requestDto, id:requestId, requestRepository: this.requestRepository}))
                .then((request) => {
                    res.json({id: request.id});
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    changeStatus(req, res) {
        let id = req.params.id;
        let status = req.params.status;

        console.log(req.params);

        return this.requestRepository.getRequestById(id)
                .then((request) => {
                    return request.changeStatus(status);
                })
                .then((result) => {
                    res.end();
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).end({error: error.message});
                });
    }

    createRowRequest(req, res) {
        let rowReqest = req.body;

        let category = this.classifier.categorize(rowReqest.data);
        console.log(category);
        return Promise.resolve()
            .then(() => Requst.createAuto({category, additionalInfo: rowReqest.data, requestRepository: this.requestRepository, categoryRepository: this.categoryRepository}))
            .then((request) => {
                res.json({id: request.id});
                res.end();
            })
            .catch(error => {
                console.error(error);
                res.status(500).end({error: error.message});
            });
    }
};