module.exports = {
    'Category': class Category {
        constructor(id, name, subcategories) {
            this.id = id;
            this.name = name;
            this.subcategories = subcategories;
        }
    },
    'SubCategory': class SubCategory {
        constructor(id, name, types, style, defaultPriority) {
            this.id = id;
            this.name = name;
            this.types = types;
            this.style = style;
            this.defaultPriority = defaultPriority;
        }
    },
    'Type': class Type {
        constructor(id, name) {
            this.id = id;
            this.name = name;
        }
    },
}